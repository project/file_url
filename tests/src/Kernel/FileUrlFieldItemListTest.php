<?php

declare(strict_types = 1);

namespace Drupal\Tests\file_url\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_test\Entity\EntityTestRev;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\file_url\Entity\RemoteFile;
use Drupal\file_url\Plugin\Field\FieldType\FileUrlFieldItemList;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Tests the overridden field item list.
 *
 * @group file_url
 * @coversDefaultClass \Drupal\file_url\Plugin\Field\FieldType\FileUrlFieldItemList
 */
class FileUrlFieldItemListTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['file', 'file_url'];

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * A local test file entity.
   */
  protected FileInterface $localFile;

  /**
   * A remote test file entity.
   */
  protected FileInterface $remoteFile;

  /**
   * Directory where the sample files are stored.
   */
  protected string $directory;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->installEntitySchema('entity_test_rev');
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);

    FieldStorageConfig::create([
      'field_name' => 'file_url_test',
      'entity_type' => 'entity_test_rev',
      'type' => 'file_url',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();
    $this->directory = $this->getRandomGenerator()->name(8);
    FieldConfig::create([
      'entity_type' => 'entity_test_rev',
      'field_name' => 'file_url_test',
      'bundle' => 'entity_test_rev',
      'settings' => ['file_directory' => $this->directory],
    ])->save();

    // Create a local test file.
    file_put_contents('public://example.txt', $this->randomMachineName());
    $this->localFile = File::create(['uri' => 'public://example.txt']);
    $this->localFile->save();

    // Create a remote test file.
    $this->remoteFile = RemoteFile::create(['uri' => $this->getRandomUrl()]);
    $this->remoteFile->save();
  }

  /**
   * Tests the retrieval of referenced files.
   *
   * @covers ::referencedEntities
   */
  public function testReferencedEntities(): void {
    // Create a test entity.
    $entity = EntityTestRev::create();

    // Check that the test file field has our overridden field item list.
    if (!$entity->file_url_test instanceof FileUrlFieldItemList) {
      $this->fail('A freshly created File Url field has the correct item list class.');
    }
    // Populate the file field with references to both a local and a remote
    // file.
    $entity->file_url_test->setValue([
      0 => ['target_id' => $this->localFile->id()],
      1 => ['target_id' => $this->remoteFile->id()],
    ]);
    $entity->name->value = $this->randomMachineName();
    $entity->save();

    $reloaded_entity = $this->reloadEntity($entity);

    // Check that the file field still has our overridden field item list after
    // loading it from the database.
    if (!$entity->file_url_test instanceof FileUrlFieldItemList) {
      $this->fail('A freshly created File Url field has the correct item list class.');
    }

    // Check that both the local and the remote file references are returned.
    $referenced_entities = $reloaded_entity->file_url_test->referencedEntities();

    $this->assertTrue(!empty($referenced_entities[0]));
    $this->assertEquals('Drupal\file\Entity\File', get_class($referenced_entities[0]));
    $this->assertEquals($this->localFile->id(), $referenced_entities[0]->id());

    $this->assertTrue(!empty($referenced_entities[1]));
    $this->assertEquals('Drupal\file_url\Entity\RemoteFile', get_class($referenced_entities[1]));
    $this->assertEquals($this->remoteFile->id(), $referenced_entities[1]->id());
  }

  /**
   * Tests the deletion of an entity and the files.
   *
   * @cover ::delete
   */
  public function testEntityDelete(): void {
    $entity = EntityTestRev::create();
    $entity->file_url_test->setValue([
      0 => ['target_id' => $this->localFile->id()],
    ]);
    $entity->set('name', $this->randomMachineName());
    $entity->save();

    $this->assertFileUsage($this->localFile, 1);
    $entity->delete();
    $this->assertFileUsage($this->localFile, 0);

    // Ensure that deleting a file does not break the deletion of the entity.
    $entity = EntityTestRev::create();
    $entity->file_url_test->setValue([
      0 => ['target_id' => $this->localFile->id()],
    ]);
    $entity->set('name', $this->randomMachineName());
    $entity->save();

    // Delete the file.
    $this->localFile->delete();
    $entity->delete();
  }

  /**
   * Tests the deletion of an entity revision and the files.
   *
   * @cover ::deleteRevision
   */
  public function testEntityRevisionDelete(): void {
    $entity = EntityTestRev::create();
    $entity->file_url_test->setValue([
      0 => ['target_id' => $this->localFile->id()],
    ]);
    $entity->set('name', $this->randomMachineName());
    $entity->save();

    // Save a second revision.
    $entity->setNewRevision(TRUE);
    $entity->save();

    $this->assertFileUsage($this->localFile, 2);
    $storage = \Drupal::entityTypeManager()->getStorage('entity_test_rev');
    $revision_ids = $storage->getQuery()
      ->allRevisions()
      ->accessCheck(FALSE)
      ->condition('id', $entity->id())
      ->execute();
    $this->assertCount(2, $revision_ids);

    // Delete the first revision.
    $revision_ids = empty($revision_ids) ? [] : array_keys($revision_ids);
    $storage->deleteRevision($revision_ids[0]);
    $this->assertFileUsage($this->localFile, 1);

    // Add a revision to ensure that deletion does not break if the file is
    // deleted prior to the revision.
    $entity->setNewRevision(TRUE);
    $entity->save();
    $this->assertFileUsage($this->localFile, 2);

    // Delete the file.
    $this->localFile->delete();
    $revision_ids = $storage->getQuery()
      ->allRevisions()
      ->accessCheck(FALSE)
      ->condition('id', $entity->id())
      ->execute();
    $revision_ids = empty($revision_ids) ? [] : array_keys($revision_ids);
    $storage->deleteRevision($revision_ids[0]);
  }

  /**
   * Returns a random URL.
   *
   * @return string
   *   The random URL.
   */
  protected function getRandomUrl(): string {
    return 'http://example.com/' . $this->randomMachineName();
  }

  /**
   * Reloads the given entity from the storage and returns it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be reloaded.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The reloaded entity.
   */
  protected function reloadEntity(EntityInterface $entity): EntityInterface {
    $controller = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $controller->resetCache([$entity->id()]);
    return $controller->load($entity->id());
  }

  /**
   * Asserts that the given file has the expected usage count.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file to check.
   * @param int $expected_count
   *   The expected usage count.
   */
  protected function assertFileUsage(File $file, int $expected_count): void {
    $usage = \Drupal::getContainer()->get('file.usage')->listUsage($file);
    $usages = empty($usage) ? 0 : $usage['file']['entity_test_rev'][$file->id()];
    $this->assertEquals($expected_count, $usages);
  }

}
